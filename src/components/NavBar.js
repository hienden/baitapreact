import React from 'react';

function NavBar() {
  return (
    <div>
        <nav className="navbar navbar-inverse">
            <div className="container-fluid">
            <div className="navbar-header">
                <a className="navbar-brand" href="#top">WebSiteName</a>
            </div>
            <ul className="nav navbar-nav">
                <li className="active"><a href="#"></a></li>
                <li className="dropdown"><a className="dropdown-toggle" data-toggle="dropdown" href="#top">Page 1 <span className="caret"></span></a>
                    <ul className="dropdown-menu">
                    <li><a href="#top">Page 1-1</a></li>
                    <li><a href="#top">Page 1-2</a></li>
                    <li><a href="#top">Page 1-3</a></li>
                    </ul>
                </li>
                <li><a href="#top">Page 2</a></li>
                <li><a href="#top">Page 3</a></li>
            </ul>
            </div>
        </nav>
    </div>
  );
}

export default NavBar;
