import React from 'react';
import logo from './logo.svg';
import './App.css';
import NavBar from './components/NavBar'
import Product from './components/Product'

function App() {
  return (
    <div className="App">
    <NavBar/>
    <Product/>
  </div>

  );
}

export default App;
